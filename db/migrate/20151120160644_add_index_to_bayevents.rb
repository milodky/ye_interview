class AddIndexToBayevents < ActiveRecord::Migration
  def change
    add_column :bay_events, :timestamp, :datetime
    add_index  :bay_events, :timestamp, :unique => true
  end
end

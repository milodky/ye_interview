class AddEventTypeToBayEvent < ActiveRecord::Migration
  def change
    add_column :bay_events, :event_type, :string
  end
end

class CreateBayEvents < ActiveRecord::Migration
  def change
    create_table :bay_events do |t|

      t.integer :bay_id
      t.string  :type
      t.timestamps null: false
    end
  end
end

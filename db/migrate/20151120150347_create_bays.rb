class CreateBays < ActiveRecord::Migration
  def change
    create_table :bays do |t|
      t.boolean :occupied
      t.boolean :out_of_service
      t.timestamps null: false
    end
  end
end

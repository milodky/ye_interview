class ChangeIndexOnBayEvent < ActiveRecord::Migration
  def change
    remove_index :bay_events, :timestamp 
    add_index    :bay_events, [:bay_id, :timestamp], :unique => true
  end
end

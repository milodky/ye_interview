class BayEvent < ActiveRecord::Base
  belongs_to :bay

  before_create :update_bay


  private
  def update_bay
    bay = Bay.find_by_id(bay_id) rescue nil
    if bay
      # raise an exception so that the consumer can be notified
      raise 'The bay is out of service' if bay.out_of_service
      bay.occupied = event_type == 'entry'
      bay.save
    else
      # how about else, create one?
      bay_hash = {:id => bay_id, :occupied => event_type == 'entry', :out_of_service => false}
      Bay.create(bay_hash)
    end
  end

end

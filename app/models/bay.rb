class Bay < ActiveRecord::Base
  has_many :bay_events
  has_many :visits




  def self.overall_occupancy
    bays = Bay.all
    page = {
        total: 0,
        occupied: 0,
        out_of_service: 0,
        bays: []
    }

    bays.each do |bay|
      page[:total] += 1
      page[:occupied] += 1 if bay[:occupied]
      page[:out_of_service] += 1 if bay[:out_of_service]
      page[:bays] << bay.detailed_occupancy
    end
    page
  end


  def detailed_occupancy
    {
        :bay     => id,
        :message => "#{occupied ? nil : 'Not'} Occupied".strip,
        :out_of_service => out_of_service
    }
  end
end

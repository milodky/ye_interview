class Visit < ActiveRecord::Base
  belongs_to :bay

  DEFAULT_PAGE = 1
  DEFAULT_SIZE = 100

  def self.full_events(params)

    page = (params[:page] || DEFAULT_PAGE).to_i
    size = (params[:size] || DEFAULT_SIZE).to_i
    order('entry_timestamp').page(page).per_page(size).map(&:attributes)

  end
end

class EventsController < ApplicationController



  def display
    @overall = Bay.overall_occupancy
    @overall[:full_events] = Visit.full_events(params.slice(:page, :size))
    render_data(200, @overall)
    # the code below failed
    # render 'display'
  rescue => er
    puts er.message
    puts er.backtrace
    render_error('sever error')
  end

  private
  def render_data(code, data)
    render text: data.to_json, status: code
  end

  def render_error(message)
    page = {
        error: message
    }
    render text: page.to_json, status: 500
  end
end
# Park Assist Rails Interview

Welcome! Here is your challenge. We expect it to take about four hours. Please ask questions.

## Problem 

Build a web application that displays real-time parking garage data. The parking events--cars entering and exiting spaces--comes from the Kafka log. You should build a local database of parking event history.

## Deliverables

Please develop the following:

 * A well-structured class hierarchy 
 * A script that can:
     * Read data from the Kafka log 
     * Convert the messages into ActiveRecord objects
     * Store the objects in a local database
 * An organized Rails app
 * A web page or web site that displays the following:
     * Current overall occupancy
     * Current occupancy of each bay
     * A scrolling list of events as they occur

## Details

The Kafka broker provides two data streams, called 'topics' in Kafka jargon: bays and bay_events. 

### Bays

A bay is one parking space in a garage. The **bays** topic sends updates about bays going in and out of service. Each bay is represented, in the Kafka stream, in the following format:

    { id: integer, occupied: boolean, out_of_service: boolean }

Kafka sends out a message on the bay topic whenever a bay goes out of service. This happens randomly.

### Bay Events 

A **bay_event** means someone parking or leaving a space. Bay events stream in order of occurrence.  *Entry events* have type set to 'entry' and *exit events* have type set to 'exit'.

    { bay_id: integer, timestamp: datetime, type: string }

### Visits

Entry and exit bay events should be appropriately matched to become **visits**. `Dwell` is the difference between the entry timestamp and exit timestamp, in minutes:
    
    { bay_id: integer, entry_timestamp: datetime, exit_timestamp: datetime, dwell: integer }

### Kafka client

The ruby client for Kafka is [poseidon](https://github.com/bpot/poseidon). The example code for 'Fetching messages from Kafka' in the [README](https://github.com/bpot/poseidon/blob/master/README.md) on github should provide enough information to get you started. The server is running on the following host/port:

 * host: 54.172.185.43
 * port: 9092

e.g.

    consumer = Poseidon::PartitionConsumer.new("my_test_consumer", '54.172.185.43', 9092, "topic1", 0, :earliest_offset)

The Kafka streams updates to bays and events. But the Kafka consumer may receive each message more than once. There may be duplicated announcements of entry and exit events and bays going in and out of service

*You do not need to run the server! The server is already running and a simulator is feeding data to it to stream.* (If you're curious, the script is in the bin/ directory :)


Please do your best to get as much done as possible today. Good Luck!

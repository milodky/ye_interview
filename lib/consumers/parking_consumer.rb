require File.dirname(__FILE__) + '/../../config/environment'

module ParkingConsumer

  def bay
    @consumer_bay ||= Poseidon::PartitionConsumer.new("my_test_consumer", '54.172.185.43', 9092, "bays", 0, :earliest_offset)
    @consumer_bay.fetch.each do |m|
      # should never use eval in any environment
      data = eval(m.value)
      begin
        # not sure why it raise an exception
        bay = Bay.find(data[:id]) rescue nil
        if bay
          data.delete(:id)
          bay.update_attributes data
        else
          Bay.create(data)
        end
      rescue => err
        $stderr.puts err.message
        $stderr.puts err.backtrace
      end

    end
  end

  def event
    @consumer_event ||= Poseidon::PartitionConsumer.new("my_test_consumer", '54.172.185.43', 9092, "bay_events", 0, :earliest_offset)
    messages  = @consumer_event.fetch
    messages.map! do |m|
      data = eval(m.value)
      # type is a keyword
      data[:event_type] = data.delete(:type)
      data
    end.sort_by! {|a| a[:timestamp]}

    last_event = nil
    messages.each do |data|
      begin
        # for the duplicate
        last_event, valid = valid_event?(last_event, data)
        # next if it is a duplicate
        next unless valid

        event = BayEvent.create(data)
        generate_visits(event) if data[:event_type] == 'exit'
      rescue SQLite3::ConstraintException
        # do nothing
      rescue => err
        $stderr.puts err.message
        $stderr.puts err.backtrace
      end


    end
  end

  def generate_data
    loop do
      bay
      event
    end
  end


  def valid_event?(last_event, message)
    if last_event.nil? || last_event != message[:event_type]
      last_event = message[:event_type]
      [last_event, true]
    else
      [last_event, false]
    end
  end

  def generate_visits(exit_event)
    enter_event = BayEvent.order(:timestamp => :desc).where(:bay_id =>  exit_event[:bay_id] , :event_type => 'entry').limit(1)[0]
    return if enter_event.nil?
    # not sure about the calculation
    dwell = (exit_event[:timestamp] - enter_event[:timestamp]) / 60

    visit = {bay_id: exit_event[:bay_id], entry_timestamp: enter_event[:timestamp],
              exit_timestamp: exit_event[:timestamp], dwell: dwell }
    Visit.create(visit)
  rescue => err
    puts err.message
    puts err.backtrace
  end


  extend self
end

if __FILE__ == $0
  ParkingConsumer.generate_data
end
